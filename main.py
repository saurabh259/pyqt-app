import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


data={}
tree = ET.parse('tdd.xml')
root = tree.getroot()
drop_down=[]
for child in root:
	# print(child.attrib['name'])
	list_x=[]
	for field in child:
		# print(field.attrib)
		list_x.append(field.attrib)
	data[child.attrib['name']] = list_x
	drop_down.append(child.attrib['name'])

class combodemo(QWidget):
	def __init__(self, parent = None):
		super(combodemo, self).__init__(parent)

		self.setGeometry(50, 50, 640, 480)
		
		self.flo = QFormLayout()

		self.cb = QComboBox()
		self.cb.addItems(drop_down)
		self.cb.currentIndexChanged.connect(self.selectionchange)

		self.phase = QLineEdit()

		self.flo.addRow('Enter Phase',self.phase)
		self.flo.addRow('Select Function',self.cb)		
		self.setLayout(self.flo)
		self.setWindowTitle("pyQT5 App")


	def selectionchange(self,i):
		for i in reversed(range(self.flo.count())):
			if i==3:
				break
			self.flo.itemAt(i).widget().setParent(None)
		print ("Current index",i,"selection changed ",self.cb.currentText())

		if len(data[self.cb.currentText()]) == 0:
			self.flo.addRow(QLabel('No Records found!'))
			return
		for row in data[self.cb.currentText()]:
			e1 = QLineEdit()
			self.flo.addRow(row['name']+' (Range : '+row['range']+')',e1)
		button = QPushButton('Save', self)
		button.setToolTip('Adds phase to XML file!')
		self.flo.addRow(button)

		button.clicked.connect(self.on_click)

	def on_click(self):

		textValues=[]
		for i in reversed(range(self.flo.count())):
			if i==1:
				break
			if isinstance(self.flo.itemAt(i).widget(), QLineEdit):
				textValues.append(self.flo.itemAt(i).widget().text())

				if self.flo.itemAt(i).widget().text()=="" or self.phase.text()=="":
					msg = QMessageBox()
					msg.setIcon(QMessageBox.Information)
					msg.setText("Don't leave the fields empty!")
					msg.exec_()
					return

		print(textValues)
		textValues.reverse()

		tree = ET.parse("sample_selftest_config.xml")
		root = tree.getroot()
		print(root)
		# ET.SubElement(root,(ET.fromstring('<user username="admin" fullname="admin" password="xx"  uid="1000"/>')))
		data0 = ET.Element("phase", {"name": self.phase.text()})
		j=0
		length = len(data[self.cb.currentText()])
		for row in data[self.cb.currentText()]:
			data1 = ET.SubElement(data0,"tdd", {"name": self.cb.currentText()})
			data2 = ET.SubElement(data1,"field",{"name":row['name'],"type":row['type']})
			data2.text = textValues[j]
			j+=1
		root.append(data0)
		tree.write("sample_selftest_config.xml")
		msg = QMessageBox()
		msg.setIcon(QMessageBox.Information)
		msg.setText("XML successfully updated!")
		msg.exec_()

def main():
   app = QApplication(sys.argv)
   ex = combodemo()
   ex.show()
   sys.exit(app.exec_())

if __name__ == '__main__':
   main()